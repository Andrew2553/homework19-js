function calculateProjectDeadline(developersSpeed, backlog, deadline) {
    const workHoursPerDay = 8;
    const workDaysPerWeek = 5;
  
    // Обчислюємо загальну швидкість команди
    const teamSpeed = developersSpeed.reduce((total, speed) => total + speed, 0);
  
    // Обчислюємо загальну кількість поінтів в беклозі
    const totalBacklogPoints = backlog.reduce((total, points) => total + points, 0);
  
    // Обчислюємо загальну кількість робочих годин до дедлайну
    const totalWorkHours = workHoursPerDay * workDaysPerWeek * Math.ceil((deadline - new Date()) / (1000 * 60 * 60 * 24));
  
    if (totalBacklogPoints <= teamSpeed * totalWorkHours) {
      const remainingDays = Math.ceil(totalBacklogPoints / teamSpeed / workHoursPerDay);
      console.log(`Усі завдання будуть успішно виконані за ${remainingDays} днів до настання дедлайну!`);
    } else {
      const additionalHours = Math.ceil((totalBacklogPoints - teamSpeed * totalWorkHours) / teamSpeed);
      console.log(`Команді розробників доведеться витратити додатково ${additionalHours} годин після дедлайну, щоб виконати всі завдання в беклозі.`);
    }
  }
  
  // Приклад використання функції
  const developersSpeed = [4, 6, 8]; // Швидкість роботи розробників
  const backlog = [50, 65, 80]; // Беклог (кількість поінтів для кожного завдання)
  const deadline = new Date('2023-06-15'); // Дедлайн проекту
  
  calculateProjectDeadline(developersSpeed, backlog, deadline);
  